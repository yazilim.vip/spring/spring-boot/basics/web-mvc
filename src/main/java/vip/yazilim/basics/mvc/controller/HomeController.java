package vip.yazilim.basics.mvc.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @RequestMapping("/")
    @ResponseBody
    public Date main() {
        return new Date();
    }

    @RequestMapping("/home")
    public String home(@RequestParam(defaultValue = "User") String name, Model model) {
        model.addAttribute("user", name);
        return "home";
    }
}
